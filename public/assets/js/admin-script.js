/*Dinamički kreirana upload polja*/
$(function(){
	//Dodaj polje
	$('#dodaj_polje').click(function(){
	  $('#skloni_polje').removeAttr('disabled');
	  var dodajPolje = this;
	  var maksPolja = 5;
	  var brojPolja = $('input.file').length;
	  //Ako polja ima manje od 5
	  if (brojPolja < maksPolja) 
	  {
	    var polje = $('.file:last');
	    polje.clone().appendTo('#fajlovi')
	    if (brojPolja == 4) 
	    {
	      $(dodajPolje).attr('disabled', 'disabled');
	    };
	  };
	});
	//Oduzmi polje
	$('#skloni_polje').click(function(){
	  if ($('#dodaj_polje').is(':disabled')){
	    $('#dodaj_polje').removeAttr('disabled');
	  };
	  var skloniPolje = this;
	  var brojPolja = $('input.file').length;

	  if (brojPolja > 1) 
	  {
	    var polje = $('.file:last');
	    polje.remove();
	    if (brojPolja == 2) 
	    {
	      $(skloniPolje).attr('disabled', 'disabled');
	    };
	  };
	});
});
//brisanje slika
$(function(){
	$('button.del-img').on('click', function(){
		var $this = $(this);
			id = $this.attr('data');
			url = $this.attr('url');
			answer = confirm('Jeste sigurni? Ovo nije moguće poništiti, čak i ako ne spremite promjene u tekstu.');
			
		if (answer) {

			$.ajax({
				type 		: 'POST',
				url 		: url,
				dataType 	: 'json',
				data 		: 'id='+id,

				success : function(data) {
					response = data;
					console.log(data.status);
					
					if (data.status == 'OK') {
						
						alert('Fotografija obrisana.');
						$this.parent().remove();
					
					}else{
						
						alert (data.status);
					}
				}
			});//end ajax()
			
		} else return false;

	})
});

//Fancybox
$(function(){ 
$("a.grouped_elements").fancybox();
});