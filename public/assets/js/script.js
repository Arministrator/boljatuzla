/*Dinamički kreirana upload polja*/
$(function(){
	//Dodaj polje
	$('#dodaj_polje').click(function(){
	  $('#skloni_polje').removeAttr('disabled');
	  var dodajPolje = this;
	  var maksPolja = 5;
	  var brojPolja = $('input.file').length;
	  //Ako polja ima manje od 5
	  if (brojPolja < maksPolja) 
	  {
	    var polje = $('.file:last');
	    polje.clone().appendTo('#fajlovi')
	    if (brojPolja == 4) 
	    {
	      $(dodajPolje).attr('disabled', 'disabled');
	    };
	  };
	});
	//Oduzmi polje
	$('#skloni_polje').click(function(){
	  if ($('#dodaj_polje').is(':disabled')){
	    $('#dodaj_polje').removeAttr('disabled');
	  };
	  var skloniPolje = this;
	  var brojPolja = $('input.file').length;

	  if (brojPolja > 1) 
	  {
	    var polje = $('.file:last');
	    polje.remove();
	    if (brojPolja == 2) 
	    {
	      $(skloniPolje).attr('disabled', 'disabled');
	    };
	  };
	});
});

/* Sortiranje postova po tagovima */
function post_tag(id, url){ 


	$('.posts')
		.empty()
		.css('opacity', '0');
    
    $.ajax({
        
        type: "POST",
        url: url,
        data: "id="+id,
        dataType : 'json',
        
       

        success: function(data)
        {
        	var content = [];
	       	var i = 0;
	        $.each(data.posts, function()
	        	
	        	{
	        		
					content[i++] = '<div class="post">';
			        content[i++] =	this.title+'<br>';
			         content[i++] =	this.body+'<br>';
			        content[i++] = '</div>';
			        //tagovi
			        $.each(this.tag, function()
	        		{
	        			content[i++] = this.name;		
	        		}); 
			        //slike
			         $.each(this.photo, function()
	        		{
	        			content[i++] = this.title;		
	        		}); 			
					
				});
			
	       
	   

			$('.posts')
				.append(content.join(''))
				.animate({
    				opacity: 1,
     			}, 200, function() {
    				// Animation complete.
  				});
	    } 
    });
	} //kraj post_tag

// $(function(){

// $('.tag').on('click', function(event){
// 		event.preventDefault();
// 		var 
// 			$this = $(this);

// 		id = $this.attr('id');
// 		console.log(id)
// 		post_tag(id,"/boljatuzla/public/ajax/post_tag");
// 	});
// });

//brisanje slika
$(function(){
	$('button.del-img').on('click', function(){
		var $this = $(this);
			id = $this.attr('data');
			url = $this.attr('url');
			answer = confirm('Jeste sigurni? Ovo nije moguće poništiti, čak i ako ne spremite promjene u tekstu.');
			
		if (answer) {

			$.ajax({
				type 		: 'POST',
				url 		: url,
				dataType 	: 'json',
				data 		: 'id='+id,

				success : function(data) {
					response = data;
					console.log(data.status);
					
					if (data.status == 'OK') {
						
						alert('Fotografija obrisana.');
						$this.parent().remove();
					
					}else{
						
						alert (data.status);
					}
				}
			});//end ajax()
			
		} else return false;

	})
});

// Fancybox
$(function(){ 
$("a.grouped_elements").fancybox();
});
//lajkovi
$(function(){
	$('.like').on('click', function(event){

		event.preventDefault();
		var $this = $(this);
			postId = $this.attr('data');
			uri = $this.attr('uri');
		if($this.attr('disabled'))
		{
			return 0;
		}
		console.log('Kliknuto dugme sa post id: '+postId+' a URI je: '+uri);
		$this.attr('disabled','true');
		$this.text('Liked!');
		$.ajax({
				type 		: 'POST',
				url 		: uri,
				dataType 	: 'json',
				data 		: 'id='+postId,

				success : function(data) {
					response = data;
					console.log(data.status);
					
					if (data.status == 'OK') {
						
						$this
							.next()
								.empty()
								.append(data.likes);
					
					}else{
						
						
					}
				}
			});//end ajax()
	});
})

// Youtube video fancybox
jQuery(document).ready(function() {

	$(".video").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'			: 640,
			'height'		: 385,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});

		return false;
	});
});