-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 21, 2012 at 04:17 PM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `boljatuzla_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` VALUES(1, 'Komentar na naslov', 1, 0, 1340213707, 1340228803);
INSERT INTO `comments` VALUES(2, 'Još jedan komentar', 1, 0, 1340213723, 1340228803);
INSERT INTO `comments` VALUES(3, 'Prvi komentar na neki naslov', 1, 0, 1340232281, 1340281380);
INSERT INTO `comments` VALUES(4, 'Drugi komentar na neki naslov', 1, 0, 1340232296, 1340281380);
INSERT INTO `comments` VALUES(5, 'Treći komentar', 1, 0, 1340232895, 1340281380);
INSERT INTO `comments` VALUES(6, 'Komentar na probni tekst.', 1, 0, 1340284108, 1340286513);
INSERT INTO `comments` VALUES(7, 'Još jedan komentar na probni tekst.', 1, 0, 1340284225, 1340286513);
INSERT INTO `comments` VALUES(8, 'Treći komentar.', 1, 0, 1340285166, 1340286513);
INSERT INTO `comments` VALUES(9, 'Komentar na prvi korisnički post.', 1, 23, 1340288015, 1340288015);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `level` int(11) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` VALUES(1, 'admin', 100, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `type` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `migration` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` VALUES('app', 'default', '002_create_posts');
INSERT INTO `migration` VALUES('app', 'default', '003_create_tags');
INSERT INTO `migration` VALUES('app', 'default', '004_create_comments');
INSERT INTO `migration` VALUES('app', 'default', '005_create_photos');
INSERT INTO `migration` VALUES('package', 'sentry', '001_install_sentry_auth');
INSERT INTO `migration` VALUES('package', 'sentry', '002_add_group_parent_column');
INSERT INTO `migration` VALUES('app', 'default', '005_create_posts_tags');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `post_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `thumb_path` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` VALUES(1, 'Kljuc_2_A_copy.jpg', 0, 'upload/files/Kljuc_2_A_copy.jpg', 'upload/files/thumbs/Kljuc_2_A_copy.jpg', 1340286647, 1340287952);
INSERT INTO `photos` VALUES(2, 'Olovo_1_B_copy.jpg', 0, 'upload/files/Olovo_1_B_copy.jpg', 'upload/files/thumbs/Olovo_1_B_copy.jpg', 1340286647, 1340287952);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `from_admin` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` VALUES(20, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem-ipsum', 1, 1, 1, 1340287899, 1340287899);
INSERT INTO `posts` VALUES(21, 'Ipsum, drugi naslov', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation <br />\r\n<br />\r\nUllamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ', 'Ipsum-drugi-naslov', 1, 1, 1, 1340287920, 1340287920);
INSERT INTO `posts` VALUES(22, 'Treći post od admina', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.', 'Treci-post-od-admina', 1, 1, 1, 1340287938, 1340287938);
INSERT INTO `posts` VALUES(23, 'Naslov korisničkog posta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br />\r\n<br />\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', 1, 1, 0, 1340287981, 1340288003);
INSERT INTO `posts` VALUES(25, 'Naslov drugog korisničkog posta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', 1, 1, 0, 1340288107, 1340288115);

-- --------------------------------------------------------

--
-- Table structure for table `posts_tags`
--

CREATE TABLE `posts_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `posts_tags`
--

INSERT INTO `posts_tags` VALUES(1, 3, 1);
INSERT INTO `posts_tags` VALUES(2, 3, 2);
INSERT INTO `posts_tags` VALUES(25, 25, 12);
INSERT INTO `posts_tags` VALUES(26, 25, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `links` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` VALUES(10, 'infrastruktura', 1, 1340288056, 1340288056);
INSERT INTO `tags` VALUES(11, ' koncerti', 1, 1340288056, 1340288056);
INSERT INTO `tags` VALUES(12, 'stranke', 1, 1340288107, 1340288107);
INSERT INTO `tags` VALUES(13, ' politika', 1, 1340288107, 1340288107);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(81) NOT NULL,
  `password_reset_hash` varchar(81) NOT NULL,
  `temp_password` varchar(81) NOT NULL,
  `remember_me` varchar(81) NOT NULL,
  `activation_hash` varchar(81) NOT NULL,
  `last_login` int(11) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `activated` tinyint(4) NOT NULL,
  `profile_fields` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES(1, 'Arministrator', 'armin.alagic@gmail.com', 'uCFzQ1giKUukzEj80be09d745548bf9c195b8711d69996c9bcf753904c301008d0a1326cb86abf45', '', '', 'nEjpG2yjeakMSmwP653df0422ad05b0eed1ea6e5b0fe9b329c68ffd0e6c9a319638ccde416582f4b', '', 1340282668, '127.0.0.1', 1340282668, 1340219628, 1, 1, '');
INSERT INTO `users` VALUES(2, 'Admiroza', 'admir.alagic@gmail.com', '7cBbbwngh1VM0zYxde5dbbb399aaea1a336a4f81298d4bdcbfa9ef1efc59b6e29f0c62d4a7835331', '', '', 'JTQYG9UGhiOZNMYw1f6ac2c693003d9a68a692e0b350f21b97044ff781c4c06e552e211723a23d5c', '', 1340228342, '127.0.0.1', 1340228342, 1340228308, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--


-- --------------------------------------------------------

--
-- Table structure for table `users_metadata`
--

CREATE TABLE `users_metadata` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_metadata`
--

INSERT INTO `users_metadata` VALUES(1, '', '');
INSERT INTO `users_metadata` VALUES(2, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_suspended`
--

CREATE TABLE `users_suspended` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(50) NOT NULL,
  `attempts` int(50) NOT NULL,
  `ip` varchar(25) NOT NULL,
  `last_attempt_at` int(11) NOT NULL,
  `suspended_at` int(11) NOT NULL,
  `unsuspend_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `users_suspended`
--

