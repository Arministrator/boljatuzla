<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '002_create_posts',
				1 => '003_create_tags',
				2 => '004_create_comments',
				3 => '005_create_photos',
				4 => '005_create_posts_tags',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
			'sentry' => 
			array(
				0 => '001_install_sentry_auth',
				1 => '002_add_group_parent_column',
			),
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
