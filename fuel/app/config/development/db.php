<?php
/**
 * The development database settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=boljatuzla_dev',
			'username'   => 'fuel',
			'password'   => 'fuel',
		),
	),
);
