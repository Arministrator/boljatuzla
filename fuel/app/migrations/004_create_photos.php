<?php

namespace Fuel\Migrations;

class Create_photos
{
	public function up()
	{
		\DBUtil::create_table('photos', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'title' => array('constraint' => 255, 'type' => 'varchar'),
			'post_id' => array('constraint' => 11, 'type' => 'int'),
			'path' => array('constraint' => 255, 'type' => 'varchar'),
			'thumb_path' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('photos');
	}
}