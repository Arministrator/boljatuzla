<?php

return array(
	'required'      => 'Polje :label je obavezno i mora imati vrijednost.',
	'min_length'    => 'Polje :label mora imati najmanje :param:1 znakova.',
	'max_length'    => 'Polje :label ne smije imati više od :param:1 znakova.',
	'exact_length'  => 'Polje :label mora biti tačno dužine :param:1 znakova.',
	'match_value'   => 'Polje :label mora imati vrijednost :param:1.',
	'match_pattern' => 'Polje :label se mora poklapati s uzorkom :param:1.',
	'match_field'   => 'Polje :label se mora poklapati s poljem :param:1.',
	'valid_email'   => 'Polje :label mora imati ispravnu email adresu.',
	'valid_emails'  => 'Polje :label mora imati listu ispravnih email adresa.',
	'valid_url'     => 'Polje :label mora imati ispravan URL.',
	'valid_ip'      => 'The field :label must contain a valid IP address.',
	'numeric_min'   => 'Najmanja brojčana vrijednost polja :label mora biti :param:1',
	'numeric_max'   => 'Najveća dozvoljena brojčana vrijednost polja :label je :param:1',
	'valid_string'  => 'Polje :label ne odgovara pravilu :rule.',
);
?>