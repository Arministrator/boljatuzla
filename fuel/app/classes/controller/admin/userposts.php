<?php

class Controller_Admin_Userposts extends Controller_Admin
{

	public function action_index()
	{
		$posts = Model_Post::find();

		if (Input::method() == 'POST') {
			$posts->where('approved', Input::post('approved'));
		}
		

		$data['posts'] = $posts->related('user')
				->where('from_admin', 0)
				->order_by('created_at', 'asc')
				->get();


		
		$this->template->title = 'Korisnički postovi';
		$this->template->content = View::forge('admin/userposts/index', $data, false);
	}
	public function action_edit($id = null)
	{
		$post = Model_Post::find()
					->where('id', $id)
					->related('tag')
					->get_one();

		$val = Model_Post::validate('edit');

		if ($val->run())
		{
			$post->title = Input::post('title');
			$post->slug = Inflector::friendly_title(Input::post('title'), $lowercase = true);
			$post->body = Input::post('body');
			$post->video= Input::post('video');
			//Upload
			$config_upload = array(
	    			'path' => DOCROOT.'upload/files/',
	    			'normalize' => true,
	    			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
	   			);

			Upload::process($config_upload);
		
			if (Upload::is_valid())
			{	
			  	Upload::save();	
			}
			//Tagovi
				if (Input::post('tag')) {
					//brisnje zareza s kraja
					$tags_str  = rtrim (Input::post('tag'), ',');
					//brisanje whitespacea izmedju zareza
					$tags = array_map('trim',explode(',',$tags_str));
					
					foreach ($tags as $key => $value) {
						
						$tag = Model_Tag::find_by_name($value);
						if ($tag && $tag != ' ') { 
							$post->tag[] = $tag;
							$tag->count++;

						}
						else
						{
							if($tag != ' ')
							{
								$new_tag = new Model_Tag;
								$new_tag->name = $value;
								$new_tag->links = 1;
								$new_tag->count++;
								$post->tag[] = $new_tag;	
							}
							
						}
					}
				
				}

			if ($post->save())
			{
				
					foreach(Upload::get_files() as $file)
				{

					Image::load($file['saved_to'].$file['saved_as'])
						->preset('img')
						->save($file['saved_to'].$file['saved_as']);
						
					Image::load($file['saved_to'].$file['saved_as'])
						->preset('thumb')
						->save($file['saved_to'].'thumbs/'.$file['saved_as']);

					
						
					$photo = Model_Photo::forge(array(
						'title' => $file['saved_as'],
						'path' => Uri::base().'upload/files/'.$file['saved_as'],
						'thumb_path' => Uri::base().'upload/files/thumbs/'.$file['saved_as'],
						'post_id' => $post->id,
						
					));

					$photo->save();
					
				}


				Session::set_flash('success', 'Izmijenjen tekst ' . $post->title.'.');

				Response::redirect('admin/userposts');
			}

			else
			{
				Session::set_flash('error', 'Došlo je do greške prilikom izmjene teksta ' . $post->title);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$post->title = $val->validated('title');
				$post->slug = $val->validated('slug');
				$post->body = $val->validated('body');

				Session::set_flash('error', $val->show_errors());
			}
			
			$this->template->set_global('post', $post, false);
		}

		
		
		
		$this->template->title = $post->title;
		$this->template->content = View::forge('admin/userposts/edit');

	}

	public function action_approve($id)
	{
		if (!$id) {
			Response::redirect('admin/userposts');
		}

		$post = Model_Post::find($id);
		$post->approved = 1;

		if ($post->save()) {
			Session::set_flash('success', 'Post odobren.');
			Response::redirect('admin/userposts');
		}
		else
		{
			Session::set_flash('error', 'Greška, post nije odobren, pokušajte opet.');
			Response::redirect('admin/userposts');
		}

	}

	public function action_delete($id)
	{
		if ($post = Model_Post::find($id))
		{
			$post->delete();

			Session::set_flash('success', 'Deleted post #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete post #'.$id);
		}

		Response::redirect('admin/userposts');
	}

}
