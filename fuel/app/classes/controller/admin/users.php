<?php

class Controller_Admin_Users extends Controller_Admin
{

	public function action_index()
	{
		

		$data['groups'] = Sentry::group()->all();
		$data['users'] = Sentry::user()->all();


		$this->template->title = 'Korisnici';
		$this->template->content = View::forge('admin/users/index', $data, false);
	}

	public function action_create_group($name, $level)
	{
		if (!$name OR !$level) {
			Session::set_flash('error', 'Fali neki od parametara');
			Response::redirect('app');
		}

		try
		{
			$group_id = Sentry::group()->create(array(
				'name' => $name,
				'level' => $level,
			));
		}
		catch (SentryGroupException $e)
		{
			Session::set_flash('error', $e->getMessage());
			Response::redirect('admin/users');
		}
	}

	public function action_delete($id)
	{
		if (!$id) {
			Response::redirect('admin/users');
		}
		
		$user_posts = Model_Post::find()
						->where('user_id', $id)
						->get();

		print_r($user_posts);

		foreach ($user_posts as $post) {
			$post->delete();
		}
		
		
		$user = Sentry::user(intval($id));
		
		//Ako pokušavaš obrisati usera sa kojim si trenutno logovan,
		//da se izbjegnu greške, prvo ga treba odjaviti.
		if ($this->current_user['id'] == $id) {
			Sentry::logout();
		}

		$delete = $user->delete();

		if ($delete) 
		{
			Session::set_flash('success', 'Korisnik obrisan');
			Response::redirect('admin/users');
		}
		else
		{
			Session::set_flas('error', 'Greška prilikom brisanja korisnika');
			Response::redirect('admin/users');
		}
		
	}

	public function action_make_admin($id)
	{
		if(!$id)
		{
			Response::redirect('admin/users');
		}

		$user = Sentry::user(intval($id));
		if ($user->add_to_group('admin')) {
			Session::set_flash('success', 'Korisnik '.$user['username'].' dodan u listu administratora');
			Response::redirect('admin/users');
		}
	}

}
