<?php
class Controller_Admin_Posts extends Controller_Admin 
{

	public function action_index()
	{
		$data['posts'] = Model_Post::find()
							->where('from_admin', 1)
							->get();
		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/posts/index', $data);

	}

	public function action_view($id = null)
	{
		$data['post'] = Model_Post::find()
						->where('id', $id)
						->related('photo')
						->get_one();

		$this->template->title = "Post, administratorski";
		$this->template->content = View::forge('admin/posts/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Post::validate('create');
			
			if ($val->run())
			{
				$post = Model_Post::forge(array(
					'title' => Input::post('title'),
					'slug' => Inflector::friendly_title(Input::post('title', false)),
					'body' => nl2br(Input::post('body')),
					'from_admin' => 1,
					'approved' => 1,
					'user_id' => $this->current_user['id']
				));

					//Upload
				$config_upload = array(
	    			'path' => DOCROOT.'upload/files/',
	    			'normalize' => true,
	    			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
	   			);


			Upload::process($config_upload);
		
			if (Upload::is_valid())
			{	
			  	Upload::save();	
			}
				
				if ($post and $post->save())
				{
					foreach(Upload::get_files() as $file)
					{
					
						Image::load($file['saved_to'].$file['saved_as'])
							->preset('img')
							->save($file['saved_to'].$file['saved_as']);
							
						Image::load($file['saved_to'].$file['saved_as'])
							->preset('thumb')
							->save($file['saved_to'].'thumbs/'.$file['saved_as']);

						
							
						$photo = Model_Photo::forge(array(
							'title' => $file['saved_as'],
							'path' => Uri::base().'upload/files/'.$file['saved_as'],
							'thumb_path' => Uri::base().'upload/files/thumbs/'.$file['saved_as'],
							'post_id' => $post->id,
							
						));

						$photo->save();
					
					}
					
					Session::set_flash('success', 'Uspješno dodan tekst '.$post->title.'.');
					Response::redirect('app');
				}
				else
				{
					Session::set_flash('error', 'Došlo je do greške prilikom kreiranja posta');
				}

				
			}
			else
			{
				Session::set_flash('error', $val->show_errors());
			}
		}

		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/posts/create');

	}

	public function action_edit($id = null)
	{
		$post = Model_Post::find()
							->where('id', $id)
							->related('photo')
							->get_one();
		

		$val = Model_Post::validate('edit');

		if ($val->run())
		{
			$post->title = Input::post('title');
			$post->slug = Inflector::friendly_title(Input::post('title'), $lowercase = true);
			$post->body = Input::post('body');
			$post->user_id = $this->current_user['id'];

				//Upload
				$config_upload = array(
	    			'path' => DOCROOT.'upload/files/',
	    			'normalize' => true,
	    			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
	   			);


			Upload::process($config_upload);
		
			if (Upload::is_valid())
			{	
			  	Upload::save();	
			}

			if ($post->save())
			{
				foreach(Upload::get_files() as $file)
					{
					
						Image::load($file['saved_to'].$file['saved_as'])
							->preset('img')
							->save($file['saved_to'].$file['saved_as']);
							
						Image::load($file['saved_to'].$file['saved_as'])
							->preset('thumb')
							->save($file['saved_to'].'thumbs/'.$file['saved_as']);

						
							
						$photo = Model_Photo::forge(array(
							'title' => $file['saved_as'],
							'path' => Uri::base().'upload/files/'.$file['saved_as'],
							'thumb_path' => Uri::base().'upload/files/thumbs/'.$file['saved_as'],
							'post_id' => $post->id,
							
						));

						$photo->save();
					}
				
				Session::set_flash('success', 'Izmijenjen tekst ' . $post->title.'.');
				Response::redirect('admin/posts');
			}

			else
			{
				Session::set_flash('error', 'Došlo je do greške prilikom izmjene teksta.');
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$post->title = $val->validated('title');
				$post->title = $val->validated('body');

				Session::set_flash('error', $val->show_errors());
			}
			
			$this->template->set_global('post', $post, false);
		}

		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/posts/edit');

	}

	public function action_delete($id = null)
	{
		if ($post = Model_Post::find($id))
		{
			$photo = Model_Photo::find('all',array('where' => array('post_id' => $id),));
			foreach($photo as $slika)
			{
				
				File::delete(DOCROOT.'/upload/files/'.$slika->title);
				File::delete(DOCROOT.'/upload/files/thumbs/'.$slika->title);
				$slika->delete();
				
			}
			
			$post->delete();
			Session::set_flash('success', 'Obrisan tekst ');
		}
	
		else
		{
			Session::set_flash('error', 'Došlo je do greške prilikom brisanja teksta.');
		}

		Response::redirect('admin/posts');

	}


}