<?php
class Controller_Admin_Comments extends Controller_Admin
{
	public function action_index()
	{
		$data['comments']= Model_Comment::find('all');
		$this->template->title = 'Korisnički komentari';
		$view = View::forge('admin/comments/index', $data, false);
		$view->set_global('users', Arr::assoc_to_keyval(Model_User::find('all'), 'id', 'username'));
		$this->template->content = $view;

	}

	public function action_user_comments()
	{
		$id = Input::post('user_id');
		$data['comments']= Model_Comment::find()->where('user_id',$id)->get();
		$this->template->title = 'Korisnički komentari';
		$view = View::forge('admin/comments/index', $data, false);
		$view->set_global('users', Arr::assoc_to_keyval(Model_User::find('all'), 'id', 'username'));
		$this->template->content = $view;
	}

	public function action_edit($id = null)
	{
		$comment = Model_Comment::find()
							->where('id', $id)
							->get_one();
		
		$val = Model_Comment::validate('edit');

		if ($val->run())
		{
			
			$comment->body = Input::post('body');
			
			if ($comment->save())
			{
				Session::set_flash('success', 'Izmijenjen komentar korisnika ' . $comment->user->username.'.');

				Response::redirect('admin/comments');
			}

			else
			{
				Session::set_flash('error', 'Došlo je do greške prilikom izmjene komentara.');
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				
				$comment->body = $val->validated('body');

				Session::set_flash('error', $val->show_errors());
			}
			
			$this->template->set_global('comment', $comment, false);
		}

		
		$this->template->title = "Izmjena komentara";
		$this->template->content = View::forge('admin/comments/edit');

	}
	
	public function action_delete($id = null)
	{
		if ($comment = Model_Comment::find($id))
		{
			$comment->delete();

			Session::set_flash('success', 'Obrisan komentar #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Došlo je do greške prilikom brisanja komentara #'.$id);
		}

		Response::redirect('admin/comments');

	}	
}

