<?php

	class Controller_Admin_Tags extends Controller_Admin
	{
		public function action_index()
		{
			$data['tags'] = Model_Tag::find('all');
			$this->template->title = 'Admin / Tagovi';
			$this->template->content = View::forge('admin/tags/index', $data);
		}
		
		public function action_delete($id)
		{
			if (!$id) {
				Response::redirect('admin/tags');
			}

			$tag = Model_Tag::find($id);
			if ($tag->delete()) {
				Session::set_flash('success', 'Tag obrisan.');
				Response::redirect('admin/tags');
			}
			else
			{
				Session::set_flash('error', 'Došlo je do greške prilikom brisanja taga. Pokušajte ponovo, ili obavijestite administratora');
				Response::redirect('admin/tags');
			}
		}

		public function action_purge()
		{
			
			if (Input::method() == 'POST') {
				
				$deleted = 0;

				$tags = Model_Tag::find()
					->related('post')
					->get();

				
				foreach ($tags as $tag) {
					if (count($tag->post) == 0) {
						$tag->delete();
						$deleted++;
					}
				}



				Session::set_flash('success', 'Višak tagova uspješno obrisan.Obrisano je ukupno '.$deleted.' tagova.');
				Response::redirect('admin/tags');
	
			}
			else
			{
				Session::set_flash('error', 'Ne možete direktno pristupati ovakvim metodama.');
				Response::redirect('admin/tags');
			}
			
		}
	}