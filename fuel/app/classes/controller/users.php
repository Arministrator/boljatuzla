<?php

class Controller_Users extends Controller_Base
{

	public function action_index()
	{
		Response::redirect('users/login');
	}

	public function action_login()
	{
		if (Input::method() == 'POST') {
			
			$val = Validation::forge();

			$val->add_field('email', 'E-mail', 'required|valid_email');
			$val->add_field('password', 'Šifra', 'required');


			if ($val->run()) 
			{
	
				try
				{
				    // log the user in
				    $valid_login = Sentry::login(Input::post('email'), Input::post('password'), true);

				    if ($valid_login)
				    {
				        Session::set_flash('success', 'Uspješno ste se prijavili');
				        Response::redirect('app');
				    }
				    else
				    {
				        Session::set_flash('error', 'Netačni podaci za prijavu. Zaboravili ste šfiru? Kliknite na link ispod');
				        Response::redirect('users/login');
				    }

				}
				catch (SentryAuthException $e)
				{
				    // issue logging in via Sentry - lets catch the sentry error thrown
				    // store/set and display caught exceptions such as a suspended user with limit attempts feature.
				    $errors = $e->getMessage();
				    Session::set_flash('error', $errors);
				    Response::redirect('users/login');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
				Response::redirect('users/login');
			}
		}

		$this->template->title = 'Users &raquo; Login';
		$this->template->content = View::forge('users/login');
	}

	public function action_logout()
	{
		Sentry::logout();
		Session::set_flash('success', 'Odjavljeni ste.');
		Response::redirect('app');
	}

	public function action_register()
	{
		
		if (Input::method() == 'POST') {
			
			$val = Validation::forge();
			
			$val->add_field('username', 'Nadimak', 'required');
			$val->add_field('email', 'E-mail', 'required|valid_email');
			$val->add_field('password', 'Šifra', 'required|min_length[5]|max_length[20]|match_field[confirm]');
			$val->add_field('confirm', 'Potvrdite šifru', 'required');
			
			if($val->run())
			{	
				try
				{
					$user = Sentry::user()
							->create(array(
								'username' => Input::post('username'),
								'email' => Input::post('email'),
								'password' => Input::post('password'),
							));
					Session::set_flash('success', 'Uspješno ste se registrovali. Možete se odmah prijaviti sa vašom email adresom i šifrom.');
					Response::redirect('users/login');

				}
				catch (SentryUserException $e)
				{
					Session::set_flash('error', $e->getMessage());
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
				Response::redirect('users/register');
			}

		}

		$this->template->title = 'Users &raquo; Register';
		$this->template->content = View::forge('users/register');
	}

	public function action_activate()
	{
		$this->template->title = 'Users &raquo; Activate';
		$this->template->content = View::forge('users/activate');
	}

	public function action_forgot_pass()
	{
		
		if (Input::method() == 'POST') {
			
			$val = Validation::forge();

			$val->add_field('email', 'E-mail adresa','required|valid_email');
			$val->add_field('password', 'Šifra', 'required|min_length[5]|max_length[20]');

			if ($val->run()) 
			{
				
				try
				{
			    // reset the password
			    $reset = Sentry::reset_password(Input::post('email'), Input::post('password'));

				    if ($reset)
				    {
				        $email = $reset['email'];
				        $link = 'boljatuzla.ba/users/reset_pass_confirm/'.$reset['link']; // adjust path as needed

				        \Package::load('email');

				        $email = Email::forge();
						$email->from('admin@boljatuzla.ba');
	    				$email->to(Input::post('email'));
	    				$email->subject('Zahtjev za ponovno postavljanje šifre');
						$data['link'] = $link;
	                 	$email->body(\View::forge('app/email/reset_pass', $data));
	                 	
	                 	/* E-mail */ 
	                 	try
					    {
							$email->send();
							Session::set_flash('success', 'Na Vaš email je poslan link putem kojeg možete potvrditi promjenu šifre. Potom se možete prijaviti sa novom šifrom.');
							Response::redirect('users/login');
							return;
							
					    }
					    	catch(\EmailSendingFailedException $e)
					    {
							Session::set_flash('error', 'Došlo je do greške prilikom izmjene vaše šifre.');
							Response::redirect('users/forgot_pass');
					    }
				    }
				    else
				    {
				        // password was not reset
				    }

				}
				catch (SentryAuthException $e)
				{
				    // issue activating the user
				    // store/set and display caught exceptions such as a user not existing or user is disabled
				    $errors = $e->getMessage();
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
				Response::redirect('users/forgot_pass');
			}
		}



		$this->template->title = 'Users &raquo; Forgot pass';
		$this->template->content = View::forge('users/forgot_pass');
	}

	public function action_change_pass()
	{
		

		if (Input::method() == 'POST') 
		{
			$val = Validation::forge();
			$val->add('oldpass', 'Stara šifra')
				->add_rule('required')
				->add_rule('min_length', 5)
				->add_rule('max_length', 20);
			

			$val->add_field('newpass', 'Nova šifra', 'required|match_field[confirm]');
			$val->add_field('confirm', 'Potvrdite šifru', 'required');



			if ($val->run()) {
				
				try
				{
				    // update the user
				    $user = Sentry::user(intval($this->current_user['id']));


				    if ($user->change_password(Input::post('newpass'), Input::post('oldpass')))
				    {
				        Sentry::logout();
				        Session::set_flash('success', 'Vaša šifra je uspješno promijenjena. Molimo prijavite se sa novom šifrom.');
				        Response::redirect('users/login');

				    }

				}
				catch (SentryUserException $e)
				{
				    Session::set_flash('error', $e->getMessage());
					Response::redirect('users/change_pass');
				}


			} else {
				Session::set_flash('error', $val->error());
				Response::redirect('users/change_pass');
			}
			

		}


		$this->template->title = 'Boljatuzla | Users | Promjena passworda';
		$this->template->content = View::forge('users/change_pass');
	}

	public function action_reset_pass_confirm($login, $hash)
	{
		try
		{
		    // confirm password reset
		    $confirm_reset = Sentry::reset_password_confirm($login, $hash);
		   

		    if ($confirm_reset)
		    {
		    	Session::set_flash('success', 'Vaša šifra je uspješno promijenjena. Možete se prijaviti sa novom šifrom.');
		    	Response::redirect('users/login');
		    }
		    
		}
		catch (SentryAuthException $e)
		{
		    // issue activating the user
		    // store/set and display caught exceptions such as a user not existing or user is disabled
		    Session::set_flash('error', 'Došlo je do greške prilikom promjene Vaše šifre. Pokušajte ponovo ili obavijestite administratora na admin@boljatuzla.ba');
		    Session::response('redirect', 'users/login');
		    Session::set_flash('error', $e->getMessage());
		}
	}

}
