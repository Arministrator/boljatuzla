<?php 
class Controller_Ajax extends Controller_Rest
{
    public function post_post_tag()
    {
        $tag_id = Input::post('id');
        $data['posts'] = Model_Post::find()
                                                        ->order_by('likes', 'desc')

                            ->where('approved', 1)
                            ->where('from_admin', 0)
                            ->related('user')
                            ->related('tag')
                            ->where('tag.id', $tag_id)
                            ->related('comment')
                            ->related('comment.user')
                            ->related('photo')
                            ->get();

        $this->response($data); 
    }


    public function post_del_img()
    {

        $id = Input::post('id');

        $photo = Model_Photo::find($id);
        
        if ($photo) {

            File::delete(DOCROOT.'/upload/files/'.$photo->title);
            File::delete(DOCROOT.'/upload/files/thumbs/'.$photo->title);

            if ($photo->delete()) {
                $data['status'] = 'OK';
            }
            else
            {
                $data['status'] = 'Fotografija pronađena, ali nije uspješno obrisana';
            }
        }
        else
        {
            $data['status'] = 'Fotografija nije pronađena';
        }

        $this->response($data);
    }   

    public function post_like()
    {   
        $id = Input::post('id');
       
        $likes = Session::get('likes');
        $likes[] = $id ;
        
        Session::set('likes',$likes);
        $post = Model_Post::find($id);
        $likes = $post->likes;
        $likes++;
        $post->likes = $likes;

        if ($post->save()) 
        {
            
            $data['status'] = 'OK';
            $data['likes'] = $likes;
        }
        else
        {
            $data['status'] = 'Greška prilikom spremanja posta';
        }
       
        $this->response($data);
    }

}   
