<?php

class Controller_Admin extends Controller_Base {

	public $template = 'admin/template';

	public function before()
	{
		parent::before();
		
		if ( ! Sentry::check() and Request::active()->action != 'login')
		{
			Response::redirect('admin/login');
		}
		if ($this->current_user and !Sentry::user(intval($this->current_user['id']))->is_admin() ) {
			
			
				Session::set_flash('error', $this->current_user['username'].', morate biti admin da biste pristupili ovom dijelu stranice.');
				Response::redirect('app');
			
			
		}
		
	}

	public function action_login()
	{
		$val = Validation::forge();

		if (Input::method() == 'POST')
		{
			$val->add('email', 'email')
			    ->add_rule('required');
			$val->add('password', 'šifra')
			    ->add_rule('required');

			if ($val->run())
			{
			
				try
				{
				    // log the user in
				    $valid_login = Sentry::login(Input::post('email'), Input::post('password'), true);

				    if ($valid_login)
				    {
				        Session::set_flash('success', 'Uspješno ste se prijavili');
				        
				        if ($this->site_down) {
				        	Response::redirect('admin');
				        }
				       
				    }
				    else
				    {
				        Session::set_flash('error', 'Netačni podaci za prijavu. Zaboravili ste šfiru? Kliknite na link ispod');
				        Response::redirect('admin/login');
				    }

				}
				catch (SentryAuthException $e)
				{
				    // issue logging in via Sentry - lets catch the sentry error thrown
				    // store/set and display caught exceptions such as a suspended user with limit attempts feature.
				    $errors = $e->getMessage();
				    Session::set_flash('error', $errors);
				    Response::redirect('admin/login');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
				Response::redirect('admin/login');

			}
		}

		$this->template->title = false;
		$this->template->content = View::forge('admin/login', array('val' => $val), false);
	}

	/**
	 * The logout action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_logout()
	{
		Sentry::logout();
		Session::set_flash('succ', 'Odjavili ste se');
		Response::redirect('app');
		
	}

	/**
	 * The index action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{
		$this->template->title = 'Dashboard';
		$this->template->content = View::forge('admin/dashboard');
	}

}

/* End of file admin.php */
