<?php

class Controller_App extends Controller_Public
{

	public function before()
	{
		parent::before();
		$visited = Session::get('visited');
		
		if (!$visited) {
			Session::set('visited', true);
			Session::set_flash('success', 'Dobrodošli na Boljatuzla.ba! Da biste ostavljali teme i komentare morate biti registrovani i prijavljeni');

		}
	}
	public function action_post($slug= null,$id = null)
	{
		if(Uri::segment(3) != null)
		{


			$data['post'] = Model_Post::find()
							->where('approved', 1)
							->where('id', $id)
							->where('from_admin', 0)
							->related('user')
							->related('tag')
							->related('comment')
							->related('comment.user')
							->related('photo')
							->order_by('created_at', 'desc')
							->get_one();
			if(isset($data['post']))
			{
				$data['admin_posts'] = Model_Post::find()
						->where('from_admin', 1)
						->order_by('created_at', 'desc')
						->get();

			$data['tags'] = Model_Tag::find('all');

			$this->template->title = 'Boljatuzla &raquo; Index';
			$this->template->content = View::forge('app/posts/view', $data, false);
			}
			else
			{
				Session::set_flash('error', 'Došlo je do greške. Sadržaj koji ste tražili ne postoji.');
				$this->template->content = null;
			}
			
		}
		else
		{
			Response::redirect('app');				

		}
	}
	public function action_index()
	{
		
		$data['posts'] = Model_Post::find()
							->where('approved', 1)
							->where('from_admin', 0)
							->related('user')
							->related('tag')
							->related('comment')
							->related('comment.user')
							->related('photo')
							->order_by('created_at', 'desc')
							->get();
		
		$data['admin_posts'] = Model_Post::find()
						->where('from_admin', 1)
						->order_by('created_at', 'desc')
						->get();

		$data['tags'] = Model_Tag::find('all');



		$this->template->title = 'Boljatuzla &raquo; Index';
		$this->template->content = View::forge('app/index', $data, false);

	}
	
	public function action_create()
	{
		if (!Sentry::check()) {
				
			Session::set_flash('error', 'Morate biti prijavljeni da biste objavili novi post');
			Response::redirect('app');
		}


		if (Input::method() == 'POST')
		{

			$val = Model_Post::validate('create');
			
			if ($val->run())
			{
				$post = Model_Post::forge(array(
					'title' => Input::post('title'),
					'body' => nl2br(Input::post('body')),
					'approved' => 0,
					'likes' => 0,
					'video' =>Input::post('video'),
					'slug' => Inflector::friendly_title(Input::post('title', false)),
					'user_id' => $this->current_user['id'],
					'from_admin' => 0
					
				));

				//Tagovi
				if (Input::post('tag'))
				{
					//brisnje zareza s kraja
					$tags_str  = rtrim (Input::post('tag'), ',');
					//brisanje whitespacea izmedju zareza
					$tags = array_map('trim',explode(',',$tags_str));
					
					//svi malim slovima
					$tags = unserialize(strtolower(serialize($tags)));
					//unikatne vrijednosti
					$tags = array_unique($tags);
					
					foreach ($tags as $key => $value)
					{
				
					$tag = Model_Tag::find_by_name($value);
					if(in_array($value,$tags))
					{

					}
						if(!empty($value))
						{
							if ($tag)
							{ 
								$post->tag[] = $tag;
								$tag->count++;
							}
						else
							{
								$new_tag = new Model_Tag;
								$new_tag->name = $value;
								$new_tag->links = 1;
								$new_tag->count++;
								$post->tag[] = $new_tag;	
							}	
						}
						
					}
				
				}

			//Upload
				$config_upload = array(
	    			'path' => DOCROOT.'upload/files/',
	    			'normalize' => true,
	    			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
	   			);


			Upload::process($config_upload);
		
			if (Upload::is_valid())
			{	
			  	Upload::save();	

			}
			
				
			if ($post and $post->save())
			{
				foreach(Upload::get_files() as $file)
			{
					
					Image::load($file['saved_to'].$file['saved_as'])
						->preset('img')
						->save($file['saved_to'].$file['saved_as']);
						
					Image::load($file['saved_to'].$file['saved_as'])
						->preset('thumb')
						->save($file['saved_to'].'thumbs/'.$file['saved_as']);

					
						
					$photo = Model_Photo::forge(array(
						'title' => $file['saved_as'],
						'path' => Uri::base().'upload/files/'.$file['saved_as'],
						'thumb_path' => Uri::base().'upload/files/thumbs/'.$file['saved_as'],
						'post_id' => $post->id,
						
					));

					$photo->save();
					
				}
					
					Session::set_flash('success', 'Dodan tekst '.$post->title.'.');

					Response::redirect('app');
				}

				else
				{
					Session::set_flash('error', 'Došlo je do greške prilikom kreiranja posta');
				}
			}
			else
			{
				Session::set_flash('error', $val->show_errors());
			}
		}

		

		$this->template->title = "Novi tekst";
		$this->template->content = View::forge('app/posts/create');

	}
	public function action_view($id = null)
	{
		$this->template->set_global('admin_posts', Model_Post::find($id));
		$this->template->title = "Post";
		$this->template->content = View::forge('app/posts/view');

	}
	public function action_edit($id = null)
	{
		$post = Model_Post::find()
					->where('id', $id)
					->related('tag')
					->get_one();

		$val = Model_Post::validate('edit');

		if ($val->run())
		{
			$post->title = Input::post('title');
			$post->slug =  Inflector::friendly_title(Input::post('title', false));
			$post->body = Input::post('body');
			$post->user_id = $this->current_user['id'];
			$post->video= Input::post('video');

			//Upload
				$config_upload = array(
	    			'path' => DOCROOT.'upload/files/',
	    			'normalize' => true,
	    			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
	   			);

			Upload::process($config_upload);
		
			if (Upload::is_valid())
			{	
			  	Upload::save();	
			}
						
			//Tagovi
				if (Input::post('tag'))
				{
					//brisnje zareza s kraja
					$tags_str  = rtrim (Input::post('tag'), ',');
					//brisanje whitespacea izmedju zareza
					$tags = array_map('trim',explode(',',$tags_str));
					
					//svi malim slovima
					$tags = unserialize(strtolower(serialize($tags)));
					//unikatne vrijednosti
					$tags = array_unique($tags);
					
					foreach ($tags as $key => $value)
					{
				
					$tag = Model_Tag::find_by_name($value);
					if(in_array($value,$tags))
					{

					}
						if(!empty($value))
						{
							if ($tag)
							{ 
								$post->tag[] = $tag;
								$tag->count++;
							}
						else
							{
								$new_tag = new Model_Tag;
								$new_tag->name = $value;
								$new_tag->links = 1;
								$new_tag->count++;
								$post->tag[] = $new_tag;	
							}	
						}
						
					}
				}

			if ($post->save())
			{
				foreach(Upload::get_files() as $file)
				{
					
					Image::load($file['saved_to'].$file['saved_as'])
						->preset('img')
						->save($file['saved_to'].$file['saved_as']);
						
						Image::load($file['saved_to'].$file['saved_as'])
						->preset('thumb')
						->save($file['saved_to'].'thumbs/'.$file['saved_as']);

					
						
						$photo = Model_Photo::forge(array(
							'title' => $file['saved_as'],
							'path' => Uri::base().'upload/files/'.$file['saved_as'],
							'thumb_path' => Uri::base().'upload/files/thumbs/'.$file['saved_as'],
							'post_id' => $post->id,
						));

						$photo->save();
					
					
				}


				Session::set_flash('success', 'Izmijenjen tekst ' . $post->title.'.');

				Response::redirect('app');
			}

			else
			{
				Session::set_flash('error', 'Došlo je do greške prilikom izmjene teksta ' . $post->title);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$post->title = $val->validated('title');
				$post->body = $val->validated('body');

				Session::set_flash('error', $val->show_errors());
			}
			
			$this->template->set_global('post', $post, false);
		}

		$admin_posts = Model_Post::find()
						->where('from_admin', 1)
						->order_by('created_at', 'desc')
						->get();
		$this->template->set_global('admin_posts', $admin_posts, false);
		
		$this->template->title = "Izmjena teksta";
		$this->template->content = View::forge('app/posts/edit');

	}
	public function action_delete($id = null)
	{
		if ($post = Model_Post::find($id))
		{
			$photo = Model_Photo::find('all',array('where' => array('post_id' => $id),));
			foreach($photo as $slika)
			{
				
				File::delete(DOCROOT.'/upload/files/'.$slika->title);
				File::delete(DOCROOT.'/upload/files/thumbs/'.$slika->title);
				$slika->delete();
				
			}
			
			$post->delete();
			Session::set_flash('success', 'Obrisan tekst ');
		}
	
		else
		{
			Session::set_flash('error', 'Došlo je do greške prilikom brisanja teksta.');
		}

		Response::redirect('app');

	}
	public function action_add_comment()
	{

		if (Input::method() == 'POST')
		{	if(!Sentry::check())
			{
				$user_id = '99999';
			}
			else
			{
				$user_id = $this->current_user['id'];
			}

			$comment = Model_Comment::forge(array(
					'body' => Input::post('comment'),
					'user_id' => $user_id,
					'post_id' => Input::post('post_id'),
				));

			if ($comment and $comment->save())
				{
					
					Session::set_flash('success', 'Dodan komentar ');

					Response::redirect('app');
				}

				else
				{
					Session::set_flash('error', 'Došlo je do greške prilikom ostavljanja komentara');
				}
			}
			else
			{
				Session::set_flash('error', $val->show_error());
			}
		}
	
	public function action_post_tags($tag_id)
	{
		$data['posts'] = Model_Post::find()
							->where('approved', 1)
							->where('from_admin', 0)
							->related('user')
							->related('tag')
							->where('tag.id', $tag_id)
							->related('comment')
							->related('comment.user')
							->related('photo')
							->order_by('likes', 'desc')
							->get();
		
		$data['admin_posts'] = Model_Post::find()
						->where('from_admin', 1)
						->order_by('created_at', 'desc')
						->get();

		$data['tags'] = Model_Tag::find('all');



		$this->template->title = 'Boljatuzla &raquo; Index';
		$this->template->content = View::forge('app/index', $data, false);
	}
		
	
}
