<?php

class Controller_Base extends Controller_Template {

	public function before()
	{
		parent::before();
		
		// Assign current_user to the instance so controllers can use it
		$this->current_user = Sentry::check() ? Sentry::user()->get() : false;

		
		
		// Set a global variable so views can use it
		View::set_global('current_user', $this->current_user);

		//Site down:
		$this->site_down = true;
		
	}

}