<?php

class Controller_Auth extends Controller_Base {

	public function before()
	{
		parent::before();
		
		
	}

	
	public function action_login()
	{
		// Already logged in
		
		$val = Validation::forge();

		if (Input::method() == 'POST') {
			
			try
			{
			    // log the user in
			    $valid_login = Sentry::login(Input::post('email'), Input::post('password'), true);

			    if ($valid_login)
			    {
			        Session::set_flash('success', 'Uspješno ste se prijavili');
			        if ($this->site_down) {
			        	Response::redirect('admin');
			        }
			        Response::redirect('app');
			    }
			    else
			    {
			        Session::set_flash('error', 'Netačni podaci za prijavu. Zaboravili ste šfiru? Kliknite na link ispod');
			        Response::redirect('users/login');
			    }

			}
			catch (SentryAuthException $e)
			{
			    // issue logging in via Sentry - lets catch the sentry error thrown
			    // store/set and display caught exceptions such as a suspended user with limit attempts feature.
			    $errors = $e->getMessage();
			    Session::set_flash('error', $errors);
			    Response::redirect('auth/login');
			}
		}

		$this->template->title = 'Korisnici &raquo; Prijava';
		$this->template->content = View::forge('auth/login');
	}

	
	

}