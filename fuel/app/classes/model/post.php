<?php

class Model_Post extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'title',
		'body',
		'slug',
		'user_id',
		'approved',
		'likes',
		'video',
		'from_admin',
		'created_at',
		'updated_at'
	);

	protected static $_belongs_to = array('user');
	protected static $_has_many = array( 'comment','photo');

	protected static $_many_many = array('tag');

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Naslov', 'required|max_length[255]');
		$val->add_field('body', 'Sadržaj', 'required');

		return $val;
	}

}
