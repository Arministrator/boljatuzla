<?php

class Model_Photo extends \Orm\Model
{
	protected static $_belongs_to = array('post');
	protected static $_properties = array(
		'id',
		'title',
		'post_id',
		'path',
		'thumb_path',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

}
