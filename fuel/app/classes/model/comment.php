<?php

class Model_Comment extends \Orm\Model
{
	
	protected static $_properties = array(
		'id',
		'body',
		'user_id',
		'post_id',
		'likes',
		'created_at',
		'updated_at'
	);

	protected static $_belongs_to = array('post', 'user');

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('body', 'Sadržaj', 'required');

		return $val;
	}
}
