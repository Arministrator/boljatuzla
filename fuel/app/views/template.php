<!DOCTYPE html>
<html lang="bs">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php 
		echo Asset::js(array(
				'http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js',
				'bootstrap.min.js',
				'script.js',
				'fancy/jquery.fancybox-1.3.4.pack.js',
				'fancy/jquery.fancybox-1.3.4.js','fancy/jquery.mousewheel-3.0.4.pack.js',
				'fancy/jquery.easing-1.3.pack.js',
				'fancy/video.js'
			));
	 ?>

	
	<?php echo Asset::css(array(
		'fancy/jquery.fancybox-1.3.4.css',
		'bootstrap.css',
		'style.css'
		)); ?>
</head>
<body>
	<div class="navbar navbar-fixed-top primnav">
		<div class="navbar-inner">
			<div class="container">
							
				<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<?php echo Html::anchor('app', 'Boljatuzla!', array('class' => 'brand')); ?>

				<div class="nav-collapse">
					

					<ul class="nav pull-right">
					
						<?php if($current_user) : ?>
						<li class="dropdown">
							<?php echo Html::anchor('#', $current_user['username'].'<b class="caret"></b>', array('data-toggle' => 'dropdown')); ?>
								<ul class="dropdown-menu">
									<li>
										<?php echo Html::anchor('users/change_pass', 'Promijenite šifru'); ?>
									</li>
									<li>
										<?php echo Html::anchor('users/logout', 'Odjava'); ?>
									</li>
								</ul>
						</li>
						<?php else : ?>
							<li class="dropdown">
							<?php echo Html::anchor('#', 'Korisnik <b class="caret"></b>', array('data-toggle' => 'dropdown')); ?>
								<ul class="dropdown-menu">
									<li>
										<?php echo Html::anchor('users/register', 'Registracija'); ?>
									</li>
									<li>
										<?php echo Html::anchor('users/login', 'Prijava'); ?>
									</li>
								</ul>
							</li>
						<?php endif; ?>
					</ul>
					<form class="navbar-search pull-right" action="">
            			<input type="text" class="search-query span3" placeholder="Pretraga">
          			</form>
				</div>							
			</div>
		</div>
	</div>
				<!-- end navbar -->
			
	<div class="container">
		<div class="row">
			<div class="span12">
				
			<?php if (Session::get_flash('success')): ?>
				<div class="alert alert-success">
  					<a class="close" data-dismiss="alert" href="#">×</a>
  					<?php echo implode('</p><p>', (array) Session::get_flash('success')); ?>
  					
				</div>
			<?php endif; ?>
			<?php if (Session::get_flash('error')): ?>
				<div class="alert alert-error">
  					<a class="close" data-dismiss="alert" href="#">×</a>
  					<?php echo implode('</p><p>', (array) Session::get_flash('error')); ?>
  					
				</div>
			<?php endif; ?>
			</div>
		</div>
		
			<?php echo $content; ?>
		
		<footer>
			Footer.
		</footer>
	</div>
	<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hr_HR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>

</html>
