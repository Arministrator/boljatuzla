<div class="row">
	<div class="span12">
	<?php echo Form::open(); ?>
		<fieldset>
			<div class="clearfix">
				<?php echo Form::label('E-mail adresa', 'email'); ?>
				<div class="input">
					<?php echo Form::input('email'); ?>
				</div>
			</div>
			<div class="clearfix">
				<?php echo Form::label('Šifra', 'password'); ?>
				<div class="input">
					<?php echo Form::password('password'); ?>
				</div>
			</div>
			<div class="form-actions">
				<?php echo Form::submit('submit', 'Prijava', array('class' => 'btn btn-success')); ?>
			</div>
		</fieldset>
	<?php echo Form::close(); ?>
	</div>
</div>