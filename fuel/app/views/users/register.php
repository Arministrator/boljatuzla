<div class="row">
	<div class="span6 offset3">
		<?php echo Form::open(array('class' => 'well form-horizontal')); ?>
			<h2>
				Registracija novog člana
			</h2>
			<hr>
			<p>
				Registracija na boljatuzla.ba je jednostavna i brza i nije ju potrebno potvrditi mailom. Međutim, morate koristiti postojeći i vlastiti email račun, jer će u slučaju gubitka passworda podaci za promjenu biti poslani na tu adresu.
			</p>
			<p>
				Polja označena zvjezdicom su obavezna.
			</p>
			<hr>
			<fieldset>

          		<div class="control-group">
          			<?php echo Form::label('Nadimak*', 'username', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::input('username', '', array('class' => 'input-xlarge')); ?>
                              <p class="help-block">
                                   Možete unijeti samo Vaše ime, ime i prezime, ili nadimak. Ne smije biti duže od 25 znakova.
                              </p>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('e-mail adresa*', 'email', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::input('email', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Šifra*', 'password', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('password', '', array('class' => 'input-xlarge')); ?>
          				<p class="help-block">
          					Šifra ne smije biti kraća od 5, a duža od 20 znakova.
          				</p>
          			</div>
          			
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Potvrdite šifru*', 'confirm', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('confirm', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Registrirajte se', array('class' => 'btn btn-success')); ?>
				</div>
			</fieldset>
		<?php echo Form::close(); ?>
	</div>
</div>