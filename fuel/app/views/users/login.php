<div class="row">
	<div class="span6 offset3">
	<?php echo Form::open(array('class' => 'well form-horizontal')); ?>
		<fieldset>
               <h2>
                    Prijava
               </h2>
               <hr>               
               <p>
                    Koristite Vašu email adresu i šifru.
               </p>
               <hr>

          		<div class="control-group">
          			<?php echo Form::label('E-mail adresa', 'email', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::input('email', Input::post('email'), array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Šifra', 'password', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('password', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Prijavite se', array('class' => 'btn btn-success')); ?>
                         <?php echo Html::anchor('users/forgot_pass', 'Zaboravili ste šifru?'); ?>
				</div>
			</fieldset>
	<?php echo Form::close(); ?>
	</div>
</div>