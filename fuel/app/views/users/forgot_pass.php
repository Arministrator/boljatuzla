<div class="row">
	<div class="span6 offset3">
	<?php echo Form::open(array('class' => 'well form-horizontal')); ?>
		<fieldset>
               <h2>
                    Zaboravili ste šifru?
               </h2>
               <hr>
          		<div class="control-group">
          			<?php echo Form::label('E-mail adresa', 'email', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::input('email', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Nova šifra', 'password', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('password', '', array('class' => 'input-xlarge')); ?>
          				<p class="help-block">
          					Novu šifru nećete moći koristiti dok ne potvrdite zahtjev za promjenom šifre, koji će stići na Vaš mail. Ukoliko se u međuvremenu prijavite sa novom šifrom, proces izmjene šifre će biti poništen.
          				</p>
          			</div>
          		</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Zatražite promjenu šifre', array('class' => 'btn btn-success')); ?>
				</div>
			</fieldset>
	<?php echo Form::close(); ?>
	</div>
</div>