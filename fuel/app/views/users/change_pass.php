<div class="row">
	<div class="span6 offset3">
		<?php echo Form::open(array('class' => 'well form-horizontal')); ?>
			<h2>
                    Promjena šifre
               </h2>
               <hr>
               <p>
                    Nova šifra mora imati namjanje 5, a najviše 20 znakova.
               </p>
               <hr>
               <fieldset>

          		<div class="control-group">
          			<?php echo Form::label('Stara šifra', 'oldpass', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('oldpass', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Nova šifra', 'newpass', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('newpass', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Potvrdite šifru', 'confirm', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('confirm', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Promijenite šifru', array('class' => 'btn btn-success')); ?>
				</div>
			</fieldset>
		<?php echo Form::close(); ?>
	</div>
</div>