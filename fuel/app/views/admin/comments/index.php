<?php echo Form::open(array('class' => 'form-stacked','method'=>'POST','action'=>'admin/comments/user_comments','enctype'=>'multipart/form-data')); ?>
<div class="clearfix">
   <?php echo Form::label('Sortiranje po korisniku', 'user_id'); ?>
 
   

	<div class="form-actions">
		 	<div class="input">
   				<?php echo Form::select('user_id', Input::post('user_id'), $users, array('class' => 'span6')); ?>
			</div>
		<?php echo Form::submit('submit', 'Samo od odabranog korisnika', array('class' => 'btn btn-primary')); ?>
		<?php echo Html::anchor('admin/comments','Svi komentari',array('class' => 'btn btn-primary'));?>
	</div>
</div>
<br>

<br>
<?php echo Form::close();?>
<table class="table table-striped">
			<thead>
				<tr>
					
					<th>
						Komentar
					</th>
					<th>	
						Korisnik
					</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($comments): ?>
					<?php foreach ($comments as $comment): ?>
						<tr>
						
							<td>
								<?php echo $comment->body; ?>
							</td>
							<td>
								<?php echo $comment->user->username; ?>
							</td>
							<td class="span4">
								<?php 
									echo   Html::anchor('admin/comments/edit/'.$comment->id, 'Izmijeni',array('class' => 'btn btn-primary'));
								 ?>

								<?php echo Html::anchor('admin/comments/delete/'.$comment->id, 'Obriši', array('class' => 'btn btn-warning', 'onclick' => "return confirm('Jeste sigurni?')")); ?>
							</td>	
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>