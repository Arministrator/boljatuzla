<?php echo Form::open(array('enctype'=>'multipart/form-data')); ?>

	<fieldset>
		
		<div class="clearfix">
			<h1><?php echo Form::label('Korisnik'); ?></h1>
				<div class="input">
				<?php echo $comment->user->username;?>
			</div>
		</div>
		<div class="clearfix">
			<?php echo Form::label('Komentar'); ?>
			<div class="input">
				<?php echo Form::textarea('body', isset($comment) ? strip_tags($comment->body) : strip_tags(Input::post('body')),array('class' => 'span8', 'rows' => '8'));	 ?>
			</div>
		</div>
		
		<div class="actions">
			<?php echo Form::submit('submit', 'Potvrdi', array('class' => 'btn btn-primary')); ?>
		</div>
	</fieldset>
<?php echo Form::close(); ?>