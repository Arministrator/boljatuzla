<div class="row">
	<div class="span12">
		<table class="table table-striped">
			<thead>
				<th>
					id
				</th>
				<th>
					name
				</th>
				<th>
					level
				</th>
				<th>
					is_admin
				</th>
				<th>
					parent
				</th>
				
			</thead>
			<tbody>
				<?php foreach ($groups as $group): ?>
					<tr>
						<td>
							<?php echo $group['id']; ?>
						</td>
						<td>
							<?php echo $group['name'] ?>
						</td>
						<td>
							<?php echo $group['level']; ?>
						</td>
						<td>
							<?php echo $group['is_admin']; ?>
						</td>
						<td>
							<?php echo $group['parent']; ?>
						</td>
						
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<table class="table table-striped">

			<thead>
				<th>
					username
				</th>
				<th>
					status
				</th>
				<th>
					Admin
				</th>
				<th>
					
				</th>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
					<tr>
						<td>
							<?php echo $user['username']; ?>
						</td>
						<td>
							<?php echo $user['status']; ?>
						</td>
						<td>
							<?php if (Sentry::user(intval($user['id']))->is_admin()): ?>
								Da
							<?php endif ?>
						</td>
						<td>
							<?php echo Html::anchor('admin/users/delete/'.$user['id'], 'Obriši', array('class' => 'btn btn-warning', 'onclick' => "return confirm('Jeste sigurni?')")); ?>
							<?php echo Html::anchor('admin/users/make_admin/'.$user['id'], 'Dodaj kao admin', array('class' => 'btn btn-warning')); ?>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>