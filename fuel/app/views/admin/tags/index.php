<div class="row">
	
	<div class="span12">

		<?php echo Form::open('admin/tags/purge'); ?>
			<fieldset>
				<div class="well">
					<p>
						Čišćenje tagova znači da će biti obrisani svi tagovi koji više nemaju povezanih postova (npr, kada se obriše user i svi njegovi postovi, ili kada se obriše neki post).
					</p>
					<p class="lead">
						Ova operacija će učitati svaki pojedinačni tag i provjeriti njegu relaciju sa postovima, tako da će zasigurno potrajati, ukoliko ima puno tagova. Nemojte zatvarati browser i nemojte ići sa ove stranice, dok ne dobijete statusni izvještaj.
					</p>
				</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Pročisti tagove', array('class' => 'btn btn-warning')) ?>
				</div>

			</fieldset>
		<?php echo Form::close() ?>

		<table class="table table-striped">
			<thead>
				<th>
					Tag
				</th>
				<th>
					
				</th>
			</thead>
			<tbody>
				<?php foreach ($tags as $tag): ?>
					<tr>
						<td>
							<?php echo $tag->name; ?>
						</td>
						<td>
							<?php echo Html::anchor('admin/tags/delete/'.$tag->id, 'Obriši', array('class' => 'btn btn-warning', 'onclick' => "return confirm('Jeste sigurni?')")); ?>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>