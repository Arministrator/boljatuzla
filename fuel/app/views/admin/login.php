<div class="row">
	<div class="span8 offset2">
		<h2>
			Boljatuzla.ba
		</h2>
		<p class="lead">
			Stranica je trenutno nedostupna za javnost, zbog održavanja. Ukoliko ste jedan od administratora, možete se prijavti niže.
		</p>

		<?php echo Form::open(array('class' => 'well form-horizontal')); ?>
		<fieldset>
               <h3>
                    Prijava
               </h3>
               <hr>               
               <p>
                    Koristite Vašu email adresu i šifru.
               </p>
               <hr>

          		<div class="control-group">
          			<?php echo Form::label('E-mail adresa', 'email', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::input('email', Input::post('email'), array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

          		<div class="control-group">
          			<?php echo Form::label('Šifra', 'password', array('class' => 'control-label')); ?>
          			<div class="controls">
          				<?php echo Form::password('password', '', array('class' => 'input-xlarge')); ?>
          			</div>
          		</div>

				<div class="form-actions">
					<?php echo Form::submit('submit', 'Prijavite se', array('class' => 'btn btn-success')); ?>
                         <?php echo Html::anchor('users/forgot_pass', 'Zaboravili ste šifru?'); ?>
				</div>
			</fieldset>
	<?php echo Form::close(); ?>

	</div>

</div>

