<?php echo Form::open(array('enctype'=>'multipart/form-data')); ?>

	<fieldset>
		
		<div class="well">
				<?php echo Form::label('Naslov', 'title'); ?>

				<div class="input">
					<?php echo Form::input('title', Input::post('title', isset($post) ? $post->title : ''), array('class' => 'span4')); ?>
				</div>

			</div>

			<div class="well">
				<?php echo Form::label('Sadržaj', 'body'); ?>

				<div class="input">
					<?php echo Form::textarea('body', Input::post('body', isset($post) ? strip_tags($post->body) : ''), array('class' => 'span8', 'rows' => 8)); ?>
				</div>

			</div>
		<div class="well">
				<h3>
					Fotografije
				</h3>
				<?php if (isset($post) AND $post->photo): ?>
					<ul class="thumbnails">
						<?php foreach ($post->photo as $photo): ?>
							 
							 <li class="span2">
							    <a href="<?php echo $photo->path;?>" class="thumbnail grouped_elements" rel="<?php echo $post->id;?>">
							     	 <?php echo Html::img($photo->thumb_path) ?>
							    </a>
							    <?php echo Form::button('obrisi_sliku', 'Brisati', array('type' => 'button', 'class' => 'del-img  btn-mini btn-danger', 'data' => $photo->id,'url'=> Uri::base().'/ajax/del_img')); ?>

							  </li>
						<?php endforeach ?>
					</ul>	
				<?php endif ?>

				<?php echo Form::label('Dodajte fotografije', 'photo'); ?>
	 				

				<div  id="fajlovi">
					<?php echo Form::file('file[]', array('class' => 'file input-block-level')); ?>
				</div>
			
				<div class="form-actions">
					<?php echo Form::button('dodaj_polje', 'Više slika', array('id' => 'dodaj_polje', 'type' => 'button', 'class' => 'btn btn-success')); ?>
					<?php echo Form::button('skloni_polje', 'Manje slika', array('id' => 'skloni_polje', 'type' => 'button', 'class' => 'btn btn-warning')); ?>
					
				</div>	
			</div>
		<div class="actions">
			<?php echo Form::submit('submit', 'Objavi', array('class' => 'btn btn-primary')); ?>

		</div>
	</fieldset>
<?php echo Form::close(); ?>