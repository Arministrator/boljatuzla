<h2>Ovo su postovi koje objavljuju administratori.</h2>
<br>
<?php if ($posts): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Naslov</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($posts as $post): ?>		<tr>

			<td><?php echo $post->title; ?></td>
			<td>
				<?php echo Html::anchor('admin/posts/view/'.$post->id, 'Pregledaj'); ?> |
				<?php echo Html::anchor('admin/posts/edit/'.$post->id, 'Izmijeni'); ?> |
				<?php echo Html::anchor('admin/posts/delete/'.$post->id, 'Obriši', array('onclick' => "return confirm('Jeste sigurni?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Posts.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/posts/create', 'Dodaj novi post', array('class' => 'btn btn-success')); ?>

</p>
