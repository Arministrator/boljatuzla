<div class="row">
	<div class="span12">
		<?php echo Form::open() ?>
			<fieldset>
				
				<div class="form-actions">
					<?php echo Form::select('approved', Input::post('approved'), array('0' => 'Draft', '1' => 'Odobreni')) ?>

					<?php echo Form::submit('submit', 'Prikaži samo odabrane', array('class' => 'btn btn-success')); ?>
				</div>
			</fieldset>
		<?php echo Form::close() ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>
						Naslov
					</th>
					<th>
						Tekst
					</th>
					<th>	
						Korisnik
					</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($posts): ?>
					<?php foreach ($posts as $post): ?>
						<tr>
							<td>	
								<?php echo $post->title; ?>
							</td>
							<td>
								<?php echo $post->body; ?>
							</td>
							<td>
								<?php echo $post->user->username; ?>
							</td>
							<td class="span3">
								<?php 
									echo !$post->approved ?  Html::anchor('admin/userposts/approve/'.$post->id, 'Odobri', array('class' => 'btn btn-success')) : false;
								 ?>
								<?php echo Html::anchor('admin/userposts/edit/'.$post->id, 'Izmijeni', array('class' => 'btn btn-primary')); ?> 

								<?php echo Html::anchor('admin/userposts/delete/'.$post->id, 'Obriši', array('class' => 'btn btn-warning', 'onclick' => "return confirm('Jeste sigurni?')")); ?>
							</td>	
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>