<h1>
	Zahtjev za promjenom šifre
</h1>
<p>
	Poštovani,
</p>
<p>
	Ovaj ste mail dobili jer ste Vi, ili neko u Vaše ime, tražio promjenu šifre za vaš profil na boljatuzla.ba
</p>
<p>
	Ukoliko želite potvrditi promjenu šifre, pratite link ispod. Ukoliko niste zatražili promjenu šifre, zanemarite ovaj mail, Vaša šifra neće biti izmijenjena.
</p>
<p>
	<?php echo $link; ?>
</p>