
<div class="row">
	<div class="span4">
			<div class="well">
				<h2>
					Napiši, zakači sliku, video!
				</h2>
				<p>
					Za tebe smo stvorili mjesto na kojem možeš da izraziš šta misliš, ili osjećaš, o trenutnim prilikama u Tuzli, u svakom smislu. 
				</p>	
				<p>
					Također, ovo je mjesto na kojem možeš da napišeš, ili pokažeš, kako da poboljšamo Tuzlu zajedno.
				</p>
				<p>
					Mi se obavezujemo da ćemo uložiti svu našu snagu u to da tvoj glas čuju oni koji ga trebaju čuti i da se svaki glas shvati ozbiljno.
				</p>

			</div>
			<div class="well">
				<h2>
					Primjeren sadržaj
				</h2>
				<p>
					Iako ćemo se suzdržavati od bilo kakvog oblika cenzure, ipak sadržaj koji javno objavljujete mora biti lišen psovki, uvreda, ili bilo kakvog govora mržnje. 
				</p>
			</div>
	</div>
	
	<div class="span8">
	<?php echo Form::open(array('class' => 'form-stacked','method'=>'POST','enctype'=>'multipart/form-data')); ?>

		<fieldset>
			<div class="well">
				<h3>
				Naslov
				</h3>
				<div class="input">
					<?php echo Form::input('title', Input::post('title', isset($post) ? $post->title : ''), array('class' => 'span4')); ?>
				</div>

			</div>

			<div class="well">
				<h3>
					Tekst
				</h3>

				<div class="input">
					<?php echo Form::textarea('body', Input::post('body', isset($post) ? strip_tags($post->body) : ''), array('class' => 'span8', 'rows' => 8)); ?>
				</div>

			</div>
			<!-- Pretvaranje niza tab objekata u string -->
			<?php  $string = array(); ?>
			<?php if(isset( $post->tag )): ?>
			<?php foreach($post->tag as $tag)
				{
					$string[] = $tag->name;
				}
			$tags = implode(',',$string); ?>
			<?php endif;?>
			
			<div class="well">
				<h3>
					Video
				</h3>
				<p>
					Ukoliko želite, možete kopirati link do nekog video clip-a (npr. sa Youtube-a) u polje niže, on će se automatski vezati i prikazati u vašem postu

				</p>

				<div class="input">
					<?php echo Form::input('video', Input::post('title', isset($post) ? $post->video : ''), array('class' => 'span4')); ?>
				</div>

			</div>

			<div class="well">
				<h3>
					Fotografije
				</h3>
				<?php if (isset($post) AND $post->photo): ?>
					<ul class="thumbnails">
						<?php foreach ($post->photo as $photo): ?>
							 
							 <li class="span2">
							    <a href="<?php echo $photo->path;?>" class="thumbnail grouped_elements" rel="<?php echo $post->id;?>">
							     	 <?php echo Html::img($photo->thumb_path) ?>
							    </a>
							    <?php echo Form::button('obrisi_sliku', 'Brisati', array('type' => 'button', 'class' => 'del-img  btn-mini btn-danger', 'data' => $photo->id,'url'=>Uri::base().'ajax/del_img')); ?>

							  </li>
						<?php endforeach ?>
					</ul>	
				<?php endif ?>
				<div  id="fajlovi">
					<?php echo Form::file('file[]', array('class' => 'file input-block-level')); ?>
				</div>
				<div class="form-actions">
				<?php echo Form::button('dodaj_polje', 'Više slika', array('id' => 'dodaj_polje', 'type' => 'button', 'class' => 'btn btn-primary')); ?>
				<?php echo Form::button('skloni_polje', 'Manje slika', array('id' => 'skloni_polje', 'type' => 'button', 'class' => 'btn btn-primary')); ?>
			</div>		
			</div>

			<div class="well">
				<h3>
					Oznake (tagovi)
				</h3>
				<div class="input">
					<?php echo Form::input('tag', Input::post('tag', isset($tags) ? $tags : ''), array('class' => 'span4')); ?>
				</div>
			</div>
			
			<div class="form-actions">
				<?php echo Form::submit('submit', 'Objavi', array('class' => 'pull-right btn btn-primary')); ?>
			</div>
			</div>
			
		</fieldset>
	<?php echo Form::close(); ?>
	</div>
</div>