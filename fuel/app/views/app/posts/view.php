
<div class="row">
	<div class="span8">
		<div class="hero-unit">
			<h1>
				Bolja Tuzla!
			</h1>
			<br>
			<p class="lead">
				Napokon te svi čuju! Kaži, napiši, zakači sliku ili video o tome kako bi ti poboljšao Tuzlu, u bilo kojem smislu.
			</p>
			<?php if ($current_user): ?>
				<p>
					<?php echo Html::anchor('app/create', 'Kaži šta misliš!', array('class' => 'btn btn-primary btn-large')); ?>
				</p>
			<?php else: ?>
			<p>
				Da bi ostavio post, moraš biti <?php echo Html::anchor('users/register', 'registrovan/a'); ?> i/ili <?php echo Html::anchor('users/login', 'prijavljen/a'); ?>. Registracija traje nekoliko sekundi i odmah ćeš biti u mogućnosti reći šta misliš!
			</p>
			<?php endif ?>
			
		</div>
	</div>
	<div class="span4">
		<div class="well">
			<h3>
				Najpopularnije teme
			</h3>
			<?php if ($tags): ?>
				<?php 
					$i = 26; 
					$lh = 30;
				?>
				<?php foreach ($tags as $tag): ?>
					<?php $lh = $i + $i/2; ?>
					<?php echo Html::anchor('app/post_tags/'.$tag->id, $tag->name, array('class' => 'label label-info','id' => $tag->id, 'style' => 'font-size:'.$i.'px; line-height:'.$lh.'px')) ?>
					<?php 
						
						$i--; 
					?>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="span4">
		<?php if ($admin_posts): ?>
			<?php foreach ($admin_posts as $a_post): ?>
				<div class="well">
					<?php if ($a_post->photo): ?>
						<?php $i=0; ?>

						<ul class="thumbnails">
						  <?php foreach ($a_post->photo as $photo): ?>
						  	<?php if ($i == 0): ?>
						  		
							  	<li style="width:92%;">
							    	<a href="<?php echo $photo->path;?>" class="thumbnail grouped_elements" rel="<?php echo $post->id;?>">
							     	 	<?php echo Html::img($photo->thumb_path, array('style' => 'width:100%;')) ?>
							    	</a>
							  	</li>
						  		
						  	<?php else: ?>
						  		<li class="span1">
							    	<a href="<?php echo $photo->path;?>" class="thumbnail grouped_elements" rel="<?php echo $post->id;?>">
							     	 	<?php echo Html::img($photo->thumb_path) ?>
							    	</a>
						  		</li>
						  	<?php endif ?>
						  	<?php $i++; ?>
						  <?php endforeach ?>
						</ul>	
					<?php endif ?>
					<h3>
						<?php echo $a_post->title; ?>
					</h3>
					<p>
						<?php echo $a_post->body; ?>
					</p>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
<div class="span8 posts">
			
			<div class="post box-round">	
				<h2>
					<?php echo $post->title; ?>
				</h2>
				<h6>
					<?php echo $post->user->username . ' u '. date('d.M.Y', $post->created_at); ?>
				</h6>
				<hr>
			
				<!-- video -->
				<?php if ($post->photo OR $post->video): ?>
					<ul class="thumbnails">
					  	<?php foreach ($post->photo as $photo): ?>
					  		
							  	<li class="span1">
							    	<a href="<?php echo $photo->path;?>" class="thumbnail grouped_elements" rel="<?php echo $post->id;?>">
							     	 	<?php echo Html::img($photo->thumb_path) ?>
							     	 	
							    	</a>
							  	</li>
					  		
					  	<?php endforeach ?>
					  	<?php if ($post->video): ?>
					  		<li class="span1">
								<?php echo Html::anchor($post->video, Asset::img('video_icon_full.jpg'), array('class' => 'video thumbnail grouped_elements')); ?>
							</li>
					  	<?php endif ?>
					</ul>
				<hr>	
				<?php endif ?>
				<div class="fb-like" data-send="true" data-width="450" data-show-faces="true"></div>
				<hr>

				
				<p>
					<?php echo $post->body; ?>
				</p>

				<div class="well">	
					Oznake:
					<?php if ($post->tag): ?>
						<?php foreach ($post->tag as $tag): ?>
							<?php echo Html::anchor('app/post_tags/'.$tag->id, $tag->name, array('class' => 'tag btn btn-mini','id' => $tag->id)) ?>
						<?php endforeach ?>
					<?php else: ?>
						Nema oznaka.
					<?php endif ?>
					

					<?php if ($current_user AND $current_user['id'] == $post->user_id): ?>
					
						<div class="btn-group pull-right">
				          <a class="btn" href="#"><i class="icon-wrench"></i> Uredi post</a>
				          <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
				          <ul class="dropdown-menu">

				            <li>
				            	<?php echo Html::anchor('app/edit/'.$post->id, 'Izmijeni'); ?>
				            </li>

				            <li>
				            	<?php echo Html::anchor('app/delete/'.$post->id, 'Obriši', array('onclick' => "return confirm('Da li ste sigurni?')")); ?>
				            </li>
				          </ul>
	        			</div>
					<?php else: ?>
					<?php if(!Session::get('likes') || !in_array($post->id,Session::get('likes'))):?>
						<div class="btn-group pull-right">
							<?php echo Html::anchor('#', '<i class="icon-heart icon-white"></i>Like!', array('class' => 'like btn btn-primary', 'data' => $post->id, 'uri' => Uri::base().'ajax/like')) ?>
							<?php echo '<span class="btn count">'. $post->likes.'</span>'; ?>
						</div>
					<?php else: ?>
						<div class="btn-group pull-right">
							<?php echo Html::anchor('#', '<i class="icon-heart icon-white"></i>Liked!', array('class' => 'btn disabled btn-primary', 'data' => $post->id, 'uri' => Uri::base().'ajax/like')) ?>
							<?php echo '<span class="btn count">'. $post->likes.'</span>'; ?>
						</div>
							<?php endif ?>
						<?php endif ?>

				</div>
					<div class="accordion-group">
	             		<div class="accordion-heading">
	                		<a class="accordion-toggle" data-toggle="collapse"  href="<?php echo '#collapse'.$post->id ?>">
	                  			Komentari <?php echo $post->comment ? '<span class="badge badge-info">'.count($post->comment).'</span>' : false; ?>
	                		</a>
	              		</div>
	              		<div id="<?php echo 'collapse'.$post->id ?>" class="accordion-body collapse" style="height: 0px; ">
	                		<div class="accordion-inner">
	                  			<?php if ($post->comment): ?>
	                  				
	                  			
	                  			<?php foreach ($post->comment as $comment): ?>
									<div class="komentar">
										<p>
											<strong><?php echo $comment->user->username . ': ' ; ?></strong><?php echo $comment->body; ?>
										</p>	
									</div>
								<?php endforeach ?>

								<?php endif ?>

								<div class="form">
									<?php echo Form::open(array('class' => 'form-stacked', 'action'=>'app/add_comment','method'=>'POST')); ?>

										<input type="hidden" name="post_id" value="<?php echo $post->id;?>">
										<div class="input">
											<?php echo Form::textarea('comment', '', array('class' => 'w-98', 'rows' => '6')); ?>
										</div>
										<div class="form-actions">
											<?php echo Form::submit('submit', 'Komentiraj', array('class' => 'btn btn-primary')); ?>

										</div>
									<?php echo Form::close(); ?>
									
								</div>
	                		</div>
	              		</div>
	            	</div>
	          

			</div>
			<?php echo Html::anchor('app','Nazad');?>
</div>

<div class="clearfix"></div>
